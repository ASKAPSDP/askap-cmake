# - Try to find log4cxx, a library for easy editing of command lines.
# Variables used by this module:
#  log4cxx_ROOT_DIR     - Readline root directory
# Variables defined by this module:
#  log4cxx_FOUND - system has log4cxx Common
#  log4cxx_INCLUDE_DIR  - the log4cxx Common/include directory (cached)
#  log4cxx_INCLUDE_DIRS - the log4cxx Common include directories
#                          (identical to log4cxx_INCLUDE_DIR)
#  log4cxx_LIBRARY      - the log4cxx common  library (cached)
#  log4cxx_LIBRARIES    - the log4cxx common library plus the libraries it 
#                          depends on

# Copyright (C) 2019
if (NOT log4cxx_ROOT_DIR)
  if (NOT $ENV{log4cxx_ROOT_DIR})
     message(WARNING "Not log4cxx_ROOT_DIR set")
  else ()
     set(log4cxx_ROOT_DIR $ENV{log4cxx_ROOT_DIR})
  endif ()
endif ()

if(NOT log4cxx_FOUND)
    message(STATUS "ROOT " ${log4cxx_ROOT_DIR})
    message(STATUS "Searching for logger.h")
	find_path(log4cxx_INCLUDE_DIR log4cxx/logger.h
		HINTS ${log4cxx_ROOT_DIR} PATH_SUFFIXES include )
    message(STATUS "include dir: " ${log4cxx_INCLUDE_DIR})	
	find_path(log4cxx_SPI_INCLUDE_DIR spi/filter.h
		HINTS ${log4cxx_ROOT_DIR} PATH_SUFFIXES include/log4cxx )
    message(STATUS "include SPI dir: " ${log4cxx_SPI_INCLUDE_DIR})	
    set (log4cxx_INCLUDE_DIRS ${log4cxx_INCLUDE_DIR} ${log4cxx_SPI_INCLUDE_DIR})

	find_library(log4cxx_LIBRARY log4cxx
		HINTS ${log4cxx_ROOT_DIR} PATH_SUFFIXES lib )
    message(STATUS "library: " ${log4cxx_LIBRARY})
    include( FindPackageHandleStandardArgs )
    FIND_PACKAGE_HANDLE_STANDARD_ARGS( log4cxx log4cxx_INCLUDE_DIR log4cxx_LIBRARY )
    if( LOG4CXX_FOUND )
        set( log4cxx_FOUND TRUE )
    endif()
endif(NOT log4cxx_FOUND)
