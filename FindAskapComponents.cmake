# - Try to find readline, a library for easy editing of command lines.
# Variables used by this module:
#  ASKAP_COMPONENTS_ROOT_DIR     - ASKAP components root directory
# Variables defined by this module:
#  ASKAP_COMPONENTS_FOUND - system has ASKAP Components Common
#  ASKAP_COMPONENTS_INCLUDE_DIR  - the ASKAP Components include directory (cached)
#  ASKAP_COMPONENTS_INCLUDE_DIRS - the ASKAP Components  include directories
#                          (identical to ASKAP_COMPONENTS_INCLUDE_DIR)
#  ASKAP_COMPONENTS_LIBRARY      - the ASKAP COMPONENTS LIBRARY  library (cached)
#  ASKAP_COMPONENTS_LIBRARIES    - the ASKAP COMPONENTS LIBRARY library plus the libraries it 
#                          depends on

# Copyright (C) 2019

if (TARGET askap::components)
    set(ASKAP_COMPONENTS_FOUND TRUE)
    return()
endif()




if(NOT ASKAP_COMPONENTS_FOUND)

	find_path(ASKAP_COMPONENTS_INCLUDE_DIR askap/components/AskapComponentImager.h
		HINTS ${ASKAP_ROOT_DIR} PATH_SUFFIXES include)
	find_library(ASKAP_COMPONENTS_LIBRARY askap_components
		HINTS ${ASKAP_COMPONENTS_ROOT_DIR} PATH_SUFFIXES lib)
	mark_as_advanced(ASKAP_COMPONENTS_INCLUDE_DIR ASKAP_COMPONENTS_LIBRARY )

	set(ASKAP_COMPONENTS_INCLUDE_DIRS ${ASKAP_COMPONENTS_INCLUDE_DIR})
	set(ASKAP_COMPONENTS_LIBRARIES ${ASKAP_COMPONENT_LIBRARY})
        if(CMAKE_VERSION VERSION_LESS "2.8.3")
		find_package_handle_standard_args(ASKAP_COMPONENTS DEFAULT_MSG ASKAP_COMPONENTS_LIBRARY ASKAP_COMPONENTS_INCLUDE_DIR)
        else ()
	   include(FindPackageHandleStandardArgs)
	   find_package_handle_standard_args(ASKAP_COMPONENTS DEFAULT_MSG ASKAP_COMPONENTS_LIBRARY ASKAP_COMPONENTS_INCLUDE_DIR)
        endif ()
	message(STATUS "base-components include dir at ${ASKAP_COMPONENTS_INCLUDE_DIRS}")

endif(NOT ASKAP_COMPONENTS_FOUND)
