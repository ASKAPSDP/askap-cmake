
macro(install_yanda_depends repo)

if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${repo})
    execute_process(COMMAND git pull 
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${repo})
    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${repo}/build)
      execute_process(COMMAND rm -rf build
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${repo}/)
    endif ()	
else ()	
    execute_process(COMMAND git clone https://bitbucket.csiro.au/scm/askapsdp/${repo}.git
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
endif ()
   
execute_process(COMMAND mkdir build
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${repo})

if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${repo}/build)
    execute_process(COMMAND cmake -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_DIR} ../
	WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${repo}/build)
    execute_process(COMMAND make install
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${repo}/build)
    set(LOFAR_ROOT_DIR ${EXTERNAL_INSTALL_DIR})

else ()
    message (FATAL, "Cannot create build directory for external")
endif()

endmacro( install_yanda_depends )

