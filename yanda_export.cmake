# Macro used by yandasoft cmake projects to generate cmake configuration files
# These generated configuration files can be used both in installation trees
# or in build trees, and thus allow different cmake projects to be build
# either separately, or as part of a single parent project

macro(yanda_export NAMESPACE)

  # Only export this project's build directory as a package when necessary,
  # which is when we are being included as a project inside another project
  # and we need to make ourselves visible to other projects
  if (NOT ${PROJECT_SOURCE_DIR} STREQUAL ${CMAKE_SOURCE_DIR})
    export(PACKAGE ${PROJECT_NAME})
    export(
      EXPORT ${PROJECT_NAME}-targets
      FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-targets.cmake"
      NAMESPACE ${NAMESPACE}::
    )
  endif()

  # Installing package configuration files
  set(CMAKEFILES_DESTINATION share/${PROJECT_NAME}/cmake)
  include(CMakePackageConfigHelpers)
  configure_package_config_file(
    cmake/${PROJECT_NAME}-config.cmake.in
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config.cmake"
    INSTALL_DESTINATION ${CMAKEFILES_DESTINATION}
  )
  write_basic_package_version_file(
      "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake"
      COMPATIBILITY AnyNewerVersion 
  )
  install(
    FILES
      ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config.cmake
      ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake
    DESTINATION ${CMAKEFILES_DESTINATION}
    COMPONENT dev
  )
  install(
    EXPORT ${PROJECT_NAME}-targets
    FILE ${PROJECT_NAME}-targets.cmake
    DESTINATION ${CMAKEFILES_DESTINATION}
    COMPONENT dev
    NAMESPACE ${NAMESPACE}::
  )
endmacro()
