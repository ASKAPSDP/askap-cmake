find_path (
    LIBZMQ_INCLUDE_DIRS
    NAMES zmq.h
    HINTS ${LIBZMQ_HOME}
    PATH_SUFFIXES include
    )

find_library (
    LIBZMQ_LIBRARIES
    NAMES zmq
    HINTS ${LIBZMQ_HOME}
    PATH_SUFFIXES lib64
    )

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
    LIBZMQ
    REQUIRED_VARS LIBZMQ_LIBRARIES LIBZMQ_INCLUDE_DIRS
    )
mark_as_advanced(
    LIBZMQ_FOUND
    LIBZMQ_LIBRARIES LIBZMQ_INCLUDE_DIRS
    )
