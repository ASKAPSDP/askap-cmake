# - Try to find readline, a library for easy editing of command lines.
# Variables used by this module:
#  DUCHAMP_ROOT_DIR     - Duchamp root directory
# Variables defined by this module:
#  DUCHAMP_FOUND - system has Duchamp
#  DUCHAMP_INCLUDE_DIR  - the  Duchamp include directory (cached)
#  DUCHAMP_LIBRARY      - the Duchamp library (cached)
#  DUCHAMP_LIBRARIES    - the Duchamp library plus the libraries it 
#                          depends on

# Copyright (C) 2019


if(NOT DUCHAMP_FOUND)

	find_path(DUCHAMP_INCLUDE_DIR duchamp/duchamp.hh
		HINTS ${DUCHAMP_ROOT_DIR} PATH_SUFFIXES include)
	find_library(DUCHAMP_LIBRARY duchamp
		HINTS ${DUCHAMP_ROOT_DIR} PATH_SUFFIXES lib)
	mark_as_advanced(DUCHAMP_INCLUDE_DIR DUCHAMP_LIBRARY )

        if(CMAKE_VERSION VERSION_LESS "2.8.3")
		find_package_handle_standard_args(DUCHAMP DEFAULT_MSG DUCHAMP_LIBRARY DUCHAMP_INCLUDE_DIR)
        else ()
	   include(FindPackageHandleStandardArgs)
	   find_package_handle_standard_args(DUCHAMP DEFAULT_MSG DUCHAMP_LIBRARY DUCHAMP_INCLUDE_DIR)
        endif ()
	message(STATUS "Duchamp include dir at ${DUCHAMP_INCLUDE_DIR}")

endif(NOT DUCHAMP_FOUND)
