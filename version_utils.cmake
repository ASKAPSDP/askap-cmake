macro(get_version_string)
    #parse the version information into pieces.

    if(${ARGV1})
        set(W_DIR ${ARGV1})
    else()
        set(W_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    endif()
    execute_process(
        COMMAND git describe --always --dirty
        WORKING_DIRECTORY ${W_DIR}
        OUTPUT_VARIABLE VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    execute_process(
        COMMAND git branch 
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_BRANCH_OUTPUT
    )
    # it's possible the following two lines may be done in one go
    string(REGEX MATCH "\\* \([^\n\r]*\)" GIT_BRANCH_TEMP "${GIT_BRANCH_OUTPUT}")
    string(REGEX REPLACE "^\\* \([^\n\r]*\)" "\\1" CURRENT_GIT_BRANCH "${GIT_BRANCH_TEMP}")
    string(REGEX REPLACE "^([0-9]+)\\..*" "\\1" VERSION_MAJOR "${VERSION}")
    string(REGEX REPLACE "^[0-9]+\\.([0-9]+).*" "\\1" VERSION_MINOR "${VERSION}")
    string(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+).*" "\\1" VERSION_PATCH "${VERSION}")
    string(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.[0-9]+(.*)" "\\1" VERSION_SHA1 "${VERSION}")
    string(TIMESTAMP BUILD_TIMESTAMP "%Y-%m-%d %H:%M" UTC)

    if (${VERSION} MATCHES "^[0-9]+\\.[0-9]+\\.[0-9]+.*$")
        set(VERSION_SHORT "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
    else ()
        set(VERSION_SHORT "0.0.0")
    endif()

    message(STATUS "Git has version      = ${VERSION}")
    message(STATUS "Short version number = ${VERSION_SHORT}")
    message(STATUS "Major version number = ${VERSION_MAJOR}")
    message(STATUS "Minor version number = ${VERSION_MINOR}")
    message(STATUS "patch number         = ${VERSION_PATCH}")
    message(STATUS "SHA number           = ${VERSION_SHA1}")
    message(STATUS "Build timestamp      = ${BUILD_TIMESTAMP}")
    message(STATUS "Current git branch   = ${CURRENT_GIT_BRANCH}")

endmacro(get_version_string)
# Uncomment to test with
# cmake -P version_utils.cmake
#get_version_string()

# get_clone_url
#
# Based on the value of $GIT_CLONE_METHOD return a suitable git clone url for the given repo name.
# This is so that the developer can choose to use access keys to avoid passwds etc by setting
#   GIT_CLONE_METHOD=SSH
# The default is assumed to be HTTP.
#
# The following example for askap-cmake repo shows the two choices:
# ssh://git@bitbucket.csiro.au:7999/askapsdp/askap-cmake.git
# https://bitbucket.csiro.au/scm/askapsdp/askap-cmake.git
macro(get_clone_url REPO_NAME)
    if (DEFINED GIT_CLONE_METHOD AND GIT_CLONE_METHOD STREQUAL "SSH")
        set("GIT_CLONE_URL_${REPO_NAME}" ssh://git@bitbucket.csiro.au:7999/askapsdp/${REPO_NAME}.git)
    else()
        set("GIT_CLONE_URL_${REPO_NAME}" https://bitbucket.csiro.au/scm/askapsdp/${REPO_NAME}.git)
    endif()
endmacro(get_clone_url)
# Uncomment to test with
# cmake -P version_utils.cmake
#set(GIT_CLONE_METHOD "HTTP")
#get_clone_url(lofar-blob)
#message(STATUS "GIT_CLONE_URL_lofar-blob = " ${GIT_CLONE_URL_lofar-blob})
