# - Try to find readline, a library for easy editing of command lines.
# Variables used by this module:
#  ACCESSORS_ROOT_DIR     - Readline root directory
# Variables defined by this module:
#  ACCESSORS_FOUND - system has ACCESSORS
#  ACCESSORS_INCLUDE_DIR  - the ACCESSORS/include directory (cached)
#  ACCESSORS_INCLUDE_DIRS - the ACCESSORS include directories
#                          (identical to ACCESSORS_INCLUDE_DIR)
#  ACCESSORS_LIBRARY      - the ACCESSORS  library (cached)
#  ACCESSORS_LIBRARIES    - the ACCESSORS library plus the libraries it 
#                          depends on

# Copyright (C) 2019

if (TARGET askap::accessors)
	set(ACCESSORS_FOUND TRUE)
	return()
endif()

if(NOT ACCESSORS_FOUND)

	find_path(ACCESSORS_IMG_INCLUDE_DIR imageaccess/ImageAccessFactory.h 
		HINTS ${ACCESSORS_ROOT_DIR} PATH_SUFFIXES include/askap/)
	find_library(ACCESSORS_LIBRARY askap_accessors
		HINTS ${ACCESSORS_ROOT_DIR} PATH_SUFFIXES lib)
	find_path(ACCESSORS_CAL_INCLUDE_DIR calibaccess/CalibAccessFactory.h 
		HINTS ${ACCESSORS_ROOT_DIR} PATH_SUFFIXES include/askap/)
	find_path(ACCESSORS_DAT_INCLUDE_DIR dataaccess/DataAdapter.h
		HINTS ${ACCESSORS_ROOT_DIR} PATH_SUFFIXES include/askap/)

	set(ACCESSORS_INCLUDE_DIRS ${ACCESSORS_IMG_INCLUDE_DIR} ${ACCESSORS_CAL_INCLUDE_DIR} ${ACCESSORS_DAT_INCLUDE_DIR} )
	set(ACCESSORS_LIBRARIES ${ACCESSORS_LIBRARY})
        if(CMAKE_VERSION VERSION_LESS "2.8.3")
		find_package_handle_standard_args(ACCESSORS DEFAULT_MSG ACCESSORS_LIBRARY ACCESSORS_IMG_INCLUDE_DIR ACCESSORS_CAL_INCLUDE_DIR ACCESSORS_DAT_INCLUDE_DIR)
        else ()
	   include(FindPackageHandleStandardArgs)
	   find_package_handle_standard_args(ACCESSORS DEFAULT_MSG ACCESSORS_LIBRARY ACCESSORS_IMG_INCLUDE_DIR ACCESSORS_CAL_INCLUDE_DIR ACCESSORS_DAT_INCLUDE_DIR)
        endif ()


endif(NOT ACCESSORS_FOUND)
