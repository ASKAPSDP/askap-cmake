# - Try to find readline, a library for easy editing of command lines.
# Variables used by this module:
#  YANDASOFT_ROOT_DIR     - Readline root directory
# Variables defined by this module:
#  YANDASOFT_FOUND - system has YANDASOFT
#  YANDASOFT_INCLUDE_DIR  - the YANDASOFT include directory (cached)
#  YANDASOFT_INCLUDE_DIRS - the YANDASOFT include directories
#                          (identical to YANDASOFT_INCLUDE_DIR)
#  YANDASOFT_LIBRARY      - the YANDASOFT library (cached)
#  YANDASOFT_LIBRARIES    - the YANDASOFT library plus the libraries it 
#                          depends on

# Copyright (C) 2019

if (TARGET askap::yandasoft)
	set(YANDASOFT_FOUND TRUE)
	return()
endif()


if(NOT YANDASOFT_FOUND)

	find_path(YANDASOFT_INCLUDE_DIR askap/askap_synthesis.h
		HINTS ${YANDASOFT_ROOT_DIR} PATH_SUFFIXES include)
	find_library(YANDASOFT_LIBRARY yanda_synthesis
		HINTS ${YANDASOFT_ROOT_DIR} PATH_SUFFIXES lib)
	mark_as_advanced(YANDASOFT_INCLUDE_DIR YANDASOFT_LIBRARY )

	set(YANDASOFT_INCLUDE_DIRS ${YANDASOFT_INCLUDE_DIR})
	set(YANDASOFT_LIBRARIES ${YANDASOFT_LIBRARY})
        if(CMAKE_VERSION VERSION_LESS "2.8.3")
	   find_package_handle_standard_args(YANDASOFT DEFAULT_MSG YANDASOFT_LIBRARY YANDASOFT_INCLUDE_DIR)
        else ()
	   include(FindPackageHandleStandardArgs)
	   find_package_handle_standard_args(YANDASOFT DEFAULT_MSG YANDASOFT_LIBRARY YANDASOFT_INCLUDE_DIR)
        endif ()
	message(STATUS "yandasoft include dir at ${YANDASOFT_INCLUDE_DIRS}")

endif(NOT YANDASOFT_FOUND)
